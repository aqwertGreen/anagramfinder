﻿using System;
using System.Collections.Generic;

namespace AnagramFinder
{
    /// <summary>
    /// Finds matching Anagrams based on the input word. 
    /// Only the 26 letters of the english alphabet are accepted in both the input and dictionary
    /// </summary>
    public class Anagram
    {
        //The ascii value of the letter a to reference other letters from
        private const int a_value = (int)'a';

        private readonly string _wordToFind;

        private readonly int[] _inputCharCounts = new int[26];

        /// <summary>
        /// Initializes a new instance of the <see cref="T:AnagramFinder.Anagram"/> class.
        /// </summary>
        /// <param name="wordToFind">Word to find.</param>
        public Anagram(string wordToFind)
        {
            _wordToFind = wordToFind ?? throw new ArgumentNullException(nameof(wordToFind));

            //set the number of times a letter occurs and place in the correct position of the array
            foreach (var letter in wordToFind)
            {
                int index = (int)letter - a_value;

                //only English, non accented words allowed
                if (index < 0 || index > 25) throw new InvalidWordException(wordToFind);

                _inputCharCounts[index]++;
            }
        }

        /// <summary>
        /// Finds any matches from the specified fromWords array.
        /// </summary>
        /// <returns>The anagram matches.</returns>
        /// <param name="fromWords">Words to find anagrams from.</param>
        public string[] Matches(string[] fromWords) 
        {
            var anagrams = new List<string>();
            var lowerInput = _wordToFind.ToLower();

            foreach (var word in fromWords)
            {
                string match = FindMatch(lowerInput, word);
                if (match != null)
                {
                    anagrams.Add(match);
                }
            }
            return anagrams.ToArray();
        }

        /// <summary>
        /// Determines whether two strings are an anagram of eachother
        /// </summary>
        /// <returns>The <paramref name="word"/> if it matches <paramref name="lowerInput"/>, or null if no matching anagram</returns>
        /// <param name="lowerInput">Lowercase input.</param>
        /// <param name="word">Word to match against</param>
         private string FindMatch(string lowerInput, string word)
        {
            if (word == lowerInput || word.Length != lowerInput.Length) return null;

            //Clone as we will be modifying the array
            int[] charCounts = (int[])_inputCharCounts.Clone();

            //For each word, subtract the number of times a letter appears 
            foreach (var letter in word)
            {
                int index = (int)letter - a_value;

                //guard against words that have characters other than the 26 english alphabet letters
                if (index < 0 || index > 25) return null;

                //If we find a position with 0 value then no letter matches input string, or have used
                // up the letter before. So no Anagram
                if (charCounts[index] == 0)
                {
                    return null;
                }
                else
                {
                    charCounts[index]--;
                }
            }

            return word;
        }
    }
}
