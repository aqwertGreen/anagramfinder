﻿using System;
namespace AnagramFinder
{
    public class InvalidWordException : Exception
    {
        public string Word { get; private set; }

        public InvalidWordException(string word)
        {
            Word = word;
        }
    }

    
}
