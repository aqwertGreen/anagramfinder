﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AnagramFinder
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Welcome to the anagram finder");
                Console.WriteLine("Press enter a word to find anagrams of");
                Console.Write(" > ");

                string input = Console.ReadLine();

                if (input == null || input.Length < 3)
                {
                    Console.WriteLine("Input cannot be empty and must be more than 2 characters");
                }
                else
                {
                    var loader = new WordLoader("words.txt");
                    var results = loader.LoadWords();

                    //Only compare against words of the same length
                    var filteredWords = results.FirstOrDefault(r => r.Length == input.Length);

                    if (filteredWords == null)
                    {
                        Console.WriteLine("No anagrams found matching " + input);
                    }
                    else
                    {
                        var finder = new Anagram(input);
                        var anagrams = finder.Matches(filteredWords.Words);

                        Console.WriteLine($"Found {anagrams.Length} anagrams for {input}");
                        Console.WriteLine(string.Join(",", anagrams));

                    }
                }

            }
            catch(Exception ex) 
            {
                Console.WriteLine(ex);
            }

            Console.ReadLine();
        }

    }
}
