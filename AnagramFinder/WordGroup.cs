﻿using System;
namespace AnagramFinder
{
    public class WordGroup
    {
        public WordGroup(int length, string[] words) 
        {
            Length = length;
            Words = words;
        }

        public int Length
        {
            get;
            private set;
        }
        public string[] Words
        {
            get;
            private set;
        }

    }
}
