﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

namespace AnagramFinder
{
    public class WordLoader
    {
        readonly string _filename;

        public WordLoader(string filename)
        {
            _filename = filename;
        }

        public IEnumerable<WordGroup> LoadWords() 
        {
            string[] words = File.ReadAllLines(_filename);

            //bucket the words into same sized collections to make it quicker to find

            var results = from w in words
                          group w by w.Length into g
                          select new WordGroup ( g.Key,  g.ToArray() );

            return results;
        }
    }



}
