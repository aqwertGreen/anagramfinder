using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using AnagramFinder;
using System;

namespace AnagramFinderTests
{
    [TestClass]
    public class AnagramTest
    {
        [TestMethod]
        public void ShouldFindNoAnagrams()
        {
            var finder = new Anagram("cat");

            var result = finder.Matches(new[] { "dog", "rat", "fish" });

            Assert.AreEqual(0, result.Length);
        }

        [TestMethod]
        public void ShouldFindNoAnagramsOnDifferentLengths()
        {
            var finder = new Anagram("cats");

            var result = finder.Matches(new[] { "cat" });

            Assert.AreEqual(0, result.Length);
        }

        [TestMethod]
        public void ShouldFindNotMatchingAnagramsIfUsingSameLetters()
        {
            var finder = new Anagram("bbtt");

            var result = finder.Matches(new[] { "bttt", "bbbt" });

            Assert.AreEqual(0, result.Length);
        }

        [TestMethod]
        public void ShouldFindMatchingAnagramsIfUsingDuplicateLetters()
        {
            var finder = new Anagram("bbtte");

            var result = finder.Matches(new[] { "ttebb", "tetbb", "aauue" });

            Assert.AreEqual(2, result.Length);
            Assert.IsTrue(result.Contains("ttebb"));
            Assert.IsTrue(result.Contains("tetbb"));
        }

        [TestMethod]
        public void ShouldFindMatchingAnagrams()
        {
            var finder = new Anagram("artz");

            var result = finder.Matches(new[] { "ratz", "tarz", "nope" });

            Assert.AreEqual(2, result.Length);
            Assert.IsTrue(result.Contains("ratz"));
            Assert.IsTrue(result.Contains("tarz"));
        }

        [TestMethod]
        public void ShouldFindNoMatchingAnagramsIfDictionayHasAccentedCharacters()
        {
            var finder = new Anagram("arts");

            var result = finder.Matches(new[] { "råts", "tårs", "nope" });

            Assert.AreEqual(0, result.Length);
        }

        [TestMethod, ExpectedException(typeof(InvalidWordException))]
        public void ShouldThrowExceptionUponInputHavingAccentedCharacters()
        {
            var finder = new Anagram("råts");
        }

        [TestMethod, ExpectedException(typeof(ArgumentNullException))]
        public void ShouldThrowExceptionUponNullInput()
        {
            var finder = new Anagram(null);
        }

    }
}